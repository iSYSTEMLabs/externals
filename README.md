# Externals

Placeholder for code from around the internet.

## Rules
1. Flat structure

   One subfolder per external

2. One folder per external version

   Keep multiple references to same external if multiple versions are needed